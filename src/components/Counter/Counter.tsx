import React, { Component, ErrorInfo } from 'react';
import Button from '../Button/Button';
import styles from './Counter.module.scss';

type counterProps = {
  counter: number;
  error: boolean;
};

export default class Counter extends Component<counterProps> {
  state: counterProps = {
    counter: 0,
    error: false
  };
  increment: () => void;
  decrement: () => void;

  constructor(props: counterProps) {
    super(props);
    this.state = {
      counter: 0,
      error: false
    };
    this.increment = () =>
      this.setState({ counter: this.state.counter > 99 ? 0 : this.state.counter + 1 });
    this.decrement = () =>
      this.setState({ counter: this.state.counter < 1 ? 0 : this.state.counter - 1 });
  }

  // called only once the component is rendered.
  // used for api calls, fetching data etc.
  componentDidMount() {
    console.log('Component did mount');
  }

  render() {
    if (this.state.error) {
      return <div>Error Occured..</div>;
    }
    return (
      <div className={styles.main}>
        <Button onClick={this.increment}>Increment</Button>
        <div className={styles.counter}>{this.state.counter}</div>
        <Button onClick={this.decrement}>Decrement</Button>
      </div>
    );
  }

  //
  componentDidUpdate() {
    console.log('Component Updated');
  }
  componentWillUnmount() {
    console.log('Component is going to Unmount');
  }

  componentDidCatch(error: Error, info: ErrorInfo) {
    console.log('Error occurred');
    this.setState({ error: true });
  }
}
